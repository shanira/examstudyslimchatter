<?php

require 'vendor/autoload.php';
include 'bootstrap.php';

use Chatter\Models\Message; 
use Chatter\Models\User;
use Chatter\Middleware\Logging; 

$app = new \Slim\App;
$app->add(new Logging()); //add the midleware (Logging) layer

$app->get('/hello/{name}', function($request, $response,$args){
    return $response->write('Hello '.$args['name']);
}); // navigate to localhost/{foldername}/hello/{name} will show "Hello name on the screen

//READ MESSAGE - get the message information from database
$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();
    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at,
            'updated_at'=>$msg->updated_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('user_id','');
    $_message = new Message();
    $_message->body = $message;
    $_message->user_id = $userid;
    $_message->save();
    
    if($_message->id){
        $payload = ['message_id' => $_message->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']);
    $message->delete();

    if($message->exists){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);
    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    $_message->body = $message;
    die($_message);
    if($_message->save()){ //אם הערך נשמר
        $payload = ['message_id' => $_message->id,"result" => "The message has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();

    Message::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

//----------------------------Users----------------------

$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email,
            'created_at'=>$usr->created_at,
            'updated_at'=>$usr->updated_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});
$app->post('/users', function($request, $response, $args){
    $username = $request->getParsedBodyParam('username','');
    $email = $request->getParsedBodyParam('email','');
    $_user = new User();
    $_user->username = $username;
    $_user->email = $email;
    $_user->save();
    
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->put('/users/{user_id}', function($request, $response,$args){
    $user = $request->getParsedBodyParam('username','');
    $_user = User::find($args['user_id']);
    $_user->username = $user;
    //die($_user);
    if($_user->save()){ //אם הערך נשמר
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();

    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

//יצירת router
$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $user = User::find($_id);
    return $response->withStatus(200)->withJson($user);
});

$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();

    if($user->exists){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);
    }
});


$app->run(); //run the application
